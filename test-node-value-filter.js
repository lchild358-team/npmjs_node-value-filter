module.exports = function(){

  const filter = require(`${__dirname}/node-value-filter.js`);

  return {
    data_constructor_N001: ()=>{
      return [
        undefined,
        null,
        '',
        [],
        {},
        0,
        0.0,
        'abc',
        [1,2,3],
        {a:1},
        123,
        123,456
      ];
    },
    test_constructor_N001: (assert, value)=>{
      assert.assertEquals(value, filter(value).get());
    },
    data_constructor_N002: ()=>{
      return [
        [()=>{},undefined],
        [()=>{return undefined;},undefined],
        [()=>{return null;},null],
        [()=>{return '';},''],
        [()=>{return 0;},0],
        [()=>{return 0.0;}, 0.0],
        [()=>{return 'abc';}, 'abc'],
        [()=>{return 123;}, 123],
        [()=>{return 123.456;}, 123.456],
        [()=>{return ()=>{return 'string';};}, 'string']
      ];
    },
    test_constructor_N002: (assert, value)=>{
      assert.assertEquals(value[1], filter(value[0]).get());
    },
    data_string_filter_N001: ()=>{
      return [
        [undefined, ''],
        [null, ''],
        [true, 'true'],
        [false, 'false'],
        [0, '0'],
        [0.0, '0'],
        [123, '123'],
        [123.456, '123.456'],
        [1e2, '100'],
        [0xff, '255'],
        [0b11, '3'],
        [0o10, '8'],
        [[1,2,3,4,5], '1,2,3,4,5'],
        [{toString: ()=>{return ''}}, ''],
        [{toString: ()=>{return 'abc'}}, 'abc'],
        ['',''],
        ['abc','abc'],
        [()=>{return 123;}, '123']
      ];
    },
    test_string_filter_N001: (assert, value)=>{
      assert.assertEquals(value[1], filter(value[0]).string().get());
    },
    data_string_filter_N002: ()=>{
      return [
        [undefined, 'default'],
        [null, 'default'],
        [true, 'true'],
        [false, 'false'],
        [0, '0'],
        [0.0, '0'],
        [123, '123'],
        [123.456, '123.456'],
        [1e2, '100'],
        [0xff, '255'],
        [0b11, '3'],
        [0o10, '8'],
        [[1,2,3,4,5], '1,2,3,4,5'],
        [{toString: ()=>{return ''}}, ''],
        [{toString: ()=>{return 'abc'}}, 'abc'],
        ['',''],
        ['abc','abc'],
        [()=>{return 123;}, '123']
      ];
    },
    test_string_filter_N002: (assert, value)=>{
      assert.assertEquals(value[1], filter(value[0]).string('default').get());
    },
    data_trim_filter_N001: ()=>{
      return [
        [undefined, undefined],
        [null, null],
        [true, true],
        [false, false],
        ['', ''],
        [' ', ''],
        ['123', '123'],
        [' 123 ', '123'],
        [0, 0],
        [0.0, 0],
        [123, 123],
        [123.456, 123.456],
        [()=>{return ' 123 '}, '123']
      ]
    },
    test_trim_filter_N001: (assert, value)=>{
      assert.assertEquals(value[1], filter(value[0]).trim().get());
    },
    data_trim_filter_N002: ()=>{
      return [
        [undefined, undefined],
        [null, null],
        [true, true],
        [false, false],
        ['', ''],
        [' ', ' '],
        ['123', '123'],
        [' 123 ', ' 123 '],
        ['"', ''],
        ['""', ''],
        ['"123"', '123'],
        ['"123', '123'],
        ['123"', '123'],
        [0, 0],
        [0.0, 0],
        [123, 123],
        [123.456, 123.456],
        [()=>{return '"abc"'}, "abc"]
      ]
    },
    test_trim_filter_N002: (assert, value)=>{
      assert.assertEquals(value[1], filter(value[0]).trim('"').get());
    },
    data_trim_filter_N003: ()=>{
      return [
        [undefined, undefined],
        [null, null],
        [true, true],
        [false, false],
        ['', ''],
        [' ', ' '],
        ['123', '123'],
        [' 123 ', ' 123 '],
        ['"', '"'],
        ['"123"', '"123"'],
        ['"123', '"123'],
        ['123"', '123"'],
        ['{', ''],
        ['{}', ''],
        ['}', ''],
        ['{123}', '123'],
        ['{123', '123'],
        ['123}', '123'],
        [0, 0],
        [0.0, 0],
        [123, 123],
        [123.456, 123.456],
        [()=>{return "{abc}"}, "abc"]
      ]
    },
    test_trim_filter_N003: (assert, value)=>{
      assert.assertEquals(value[1], filter(value[0]).trim(/^{|}$/gm).get());
    },
    data_trim_filter_N004: ()=>{
      return [
        [undefined, undefined],
        [null, null],
        [true, true],
        [false, false],
        ['', ''],
        [' ', ' '],
        ['123', '123'],
        [' 123 ', ' 123 '],
        ['"', '"'],
        ['"123"', '"123"'],
        ['"123', '"123'],
        ['123"', '123"'],
        ['{', ''],
        ['{}', ''],
        ['}', ''],
        ['{123}', '123'],
        ['{123', '123'],
        ['123}', '123'],
        [0, 0],
        [0.0, 0],
        [123, 123],
        [123.456, 123.456],
        [()=>{return "{abc}"}, "abc"]
      ]
    },
    test_trim_filter_N004: (assert, value)=>{
      assert.assertEquals(value[1], filter(value[0]).trim('{','}').get());
    },
    data_number_filter_N001: ()=>{
      return [
        [undefined, 0],
        [null, 0],
        [true, 1],
        [false, 0],
        ['', 0],
        [' ', 0],
        ['abc', 0],
        ['123', 123],
        ['123.456', 123.456],
        ['-123', -123],
        ['-123.456', -123.456],
        ['+123', 123],
        ['+123.456', 123.456],
        ['0xff', 255],
        ['0b11', 3],
        ['0o10', 8],
        ['1e2', 100],
        [0, 0],
        [0.0, 0],
        [123, 123],
        [123.456, 123.456],
        [[], 0],
        [[' '], 0],
        [{}, 0],
        [[1],1],
        [[1,2],0],
        [{toString: ()=>{return ' ';}}, 0],
        [{toString: ()=>{return '123';}}, 123],
        [()=>{return '123';}, 123]
      ];
    },
    test_number_filter_N001: (assert, value)=>{
      assert.assertEquals(value[1], filter(value[0]).number().get());
    },
    data_number_filter_N002: ()=>{
      return [
        [undefined, -1],
        [null, -1],
        [true, 1],
        [false, 0],
        ['', -1],
        [' ', -1],
        ['abc', -1],
        ['123', 123],
        ['123.456', 123.456],
        ['-123', -123],
        ['-123.456', -123.456],
        ['+123', 123],
        ['+123.456', 123.456],
        ['0xff', 255],
        ['0b11', 3],
        ['0o10', 8],
        ['1e2', 100],
        [0, 0],
        [0.0, 0],
        [123, 123],
        [123.456, 123.456],
        [[], -1],
        [[' '], -1],
        [{}, -1],
        [[1],1],
        [{toString: ()=>{return ' ';}}, -1],
        [{toString: ()=>{return '123';}}, 123],
        [()=>{return '123';}, 123]
      ];
    },
    test_number_filter_N002: (assert, value)=>{
      assert.assertEquals(value[1], filter(value[0]).number(-1).get());
    },
    data_int_filter_N001: ()=>{
      return [
        [undefined, 0],
        [null, 0],
        [true, 1],
        [false, 0],
        ['', 0],
        [' ', 0],
        ['abc', 0],
        ['123', 123],
        ['123.456', 123],
        ['-123', -123],
        ['-123.456', -123],
        ['+123', 123],
        ['+123.456', 123],
        ['0xff', 0],
        ['0b11', 0],
        ['0o10', 0],
        ['1e2', 1],
        [0, 0],
        [0.0, 0],
        [123, 123],
        [123.456, 123],
        [[], 0],
        [[' '], 0],
        [{}, 0],
        [[1],1],
        [[1,2], 1],
        [{toString: ()=>{return ' ';}}, 0],
        [{toString: ()=>{return '123';}}, 123],
        [()=>{return '123';}, 123]
      ];
    },
    test_int_filter_N001: (assert, value)=>{
      assert.assertEquals(value[1], filter(value[0]).int().get());
    },
    data_int_filter_N002: ()=>{
      return [
        [undefined, 0],
        [null, 0],
        [true, 1],
        [false, 0],
        ['', 0],
        [' ', 0],
        ['abc', 0],
        ['123', 123],
        ['123.456', 123],
        ['-123', -123],
        ['-123.456', -123],
        ['+123', 123],
        ['+123.456', 123],
        ['0xff', 0],
        ['0b11', 0],
        ['0o10', 0],
        ['1e2', 1],
        [0, 0],
        [0.0, 0],
        [123, 123],
        [123.456, 123],
        [[], 0],
        [[' '], 0],
        [{}, 0],
        [[1],1],
        [[1,2], 1],
        [{toString: ()=>{return ' ';}}, 0],
        [{toString: ()=>{return '123';}}, 123],
        [()=>{return '123';}, 123]
      ];
    },
    test_int_filter_N002: (assert, value)=>{
      assert.assertEquals(value[1], filter(value[0]).int(10).get());
    },
    data_int_filter_N003: ()=>{
      return [
        [undefined, -1],
        [null, -1],
        [true, 1],
        [false, 0],
        ['', -1],
        [' ', -1],
        ['abc', -1],
        ['123', 123],
        ['123.456', 123],
        ['-123', -123],
        ['-123.456', -123],
        ['+123', 123],
        ['+123.456', 123],
        ['0xff', 0],
        ['0b11', 0],
        ['0o10', 0],
        ['1e2', 1],
        [0, 0],
        [0.0, 0],
        [123, 123],
        [123.456, 123],
        [[], -1],
        [[' '], -1],
        [{}, -1],
        [[1],1],
        [{toString: ()=>{return ' ';}}, -1],
        [{toString: ()=>{return '123';}}, 123],
        [()=>{return '123';}, 123]
      ];
    },
    test_int_filter_N003: (assert, value)=>{
      assert.assertEquals(value[1], filter(value[0]).int(10, -1).get());
    },
    data_int_filter_N004: ()=>{
      return [
        [undefined, -1],
        [null, -1],
        [true, 1],
        [false, 0],
        ['', -1],
        [' ', -1],
        ['ff', 255],
        ['11', 17],
        ['zzz', -1],
        ['11.456', 17],
        ['-11', -17],
        ['-11.456', -17],
        ['+11', 17],
        ['+11.456', 17],
        ['0xff', 255],
        ['0b1', 177],
        ['0o1', 0],
        ['0e1', 225],
        [0, 0],
        [0.0, 0],
        [123, 123],
        [123.456, 123],
        [[], -1],
        [[' '], -1],
        [{}, -1],
        [[11],17],
        [{toString: ()=>{return ' ';}}, -1],
        [{toString: ()=>{return '11';}}, 17],
        [()=>{return '11';}, 17]
      ];
    },
    test_int_filter_N004: (assert, value)=>{
      assert.assertEquals(value[1], filter(value[0]).int(16, -1).get());
    },
    data_float_filter_N001: ()=>{
      return [
        [undefined, 0],
        [null, 0],
        [true, 1],
        [false, 0],
        ['', 0],
        [' ', 0],
        ['abc', 0],
        ['123', 123],
        ['123.456', 123.456],
        ['-123', -123],
        ['-123.456', -123.456],
        ['+123', 123],
        ['+123.456', 123.456],
        ['0xff', 0],
        ['0b11', 0],
        ['0o10', 0],
        ['1e2', 100],
        [0, 0],
        [0.0, 0],
        [123, 123],
        [123.456, 123.456],
        [[], 0],
        [[' '], 0],
        [{}, 0],
        [[1.1],1.1],
        [[1.1,2], 1.1],
        [{toString: ()=>{return ' ';}}, 0],
        [{toString: ()=>{return '123.456';}}, 123.456],
        [()=>{return '123.456';}, 123.456]
      ];
    },
    test_float_filter_N001: (assert, value)=>{
      assert.assertEquals(value[1], filter(value[0]).float().get());
    },
    data_float_filter_N002: ()=>{
      return [
        [undefined, -1.1],
        [null, -1.1],
        [true, 1],
        [false, 0],
        ['', -1.1],
        [' ', -1.1],
        ['abc', -1.1],
        ['123', 123],
        ['123.456', 123.456],
        ['-123', -123],
        ['-123.456', -123.456],
        ['+123', 123],
        ['+123.456', 123.456],
        ['0xff', 0],
        ['0b11', 0],
        ['0o10', 0],
        ['1e2', 100],
        [0, 0],
        [0.0, 0],
        [123, 123],
        [123.456, 123.456],
        [[], -1.1],
        [[' '], -1.1],
        [{}, -1.1],
        [[1.1], 1.1],
        [[1.1,2], 1.1],
        [{toString: ()=>{return ' ';}}, -1.1],
        [{toString: ()=>{return '123.456';}}, 123.456],
        [()=>{return '123.456';}, 123.456]
      ];
    },
    test_float_filter_N002: (assert, value)=>{
      assert.assertEquals(value[1], filter(value[0]).float(-1.1).get());
    },
    test_custom_filter_N001: (assert)=>{
      assert.assertEquals(4, filter(2).custom((v)=>{return v * 2}).get());
      assert.assertEquals(4, filter([2]).custom(function(v){return this.number().get() * 2}).get());
    },
    test_custom_filter_N002: (assert)=>{
      let o = {value: 2, dbl: function(){return this.value * 2;}, toString: function(){return this.value.toString();}};
      assert.assertEquals(16, filter(o).custom(function(v){return this.dbl() * v.dbl()}, o).get());
    },
    test_bind_N001: (assert)=>{
      filter.bind('a', function(v, a){
        return this._value + v + a;
      }).bind('b', function(v, b){
        return v * b;
      });

      assert.assertEquals(28, filter(2).a(3).b(4).get());
    },
    test_bind_N002: (assert)=>{
      var o = [1,2,3];

      filter.bind('a', function(v){
        return this.join(';');
      }, o);

      assert.assertEquals('1;2;3', filter().a().get());
    }
  };
};
