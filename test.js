const Testsuite = require('node-unit-test');
const testsuite = new Testsuite();

let rs = testsuite
.addFile(`${__dirname}/test-node-value-filter.js`)
.execute();

process.exit(rs ? 0 : 1);

