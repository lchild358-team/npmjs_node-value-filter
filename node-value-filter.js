module.exports = exports = (function(){
 
  const Filter = function(){
    this._rules = [];
  };

  Filter.prototype.add = function(cond, exec){
    this._rules[this._rules.length] = {cond: cond, exec: exec};
    return this;
  };

  Filter.prototype.apply = function(value){
    for(var rule of this._rules){
      if(rule.cond(value)) return rule.exec(value);
    }
    return value;
  };

  /**
   * Filters
   */
  const Filters = function(value){

    while(typeof value === 'function'){
      value = value();
    }

    this._value = value;
  };

  /**
   *
   */
  Filters.prototype.get = function(){
    return this._value;
  };

  /**
   *
   */
  Filters.prototype.string = function(def){
    let f = new Filter();

    this._value = f.add(
      (v)=>{ return v === undefined || v === null; },
      (v)=>{ return typeof(def) === 'string' ? def : ''; }
    ).add(
      (v)=>{ return typeof(v) === 'boolean'; },
      (v)=>{ return v ? 'true' : 'false'; }
    ).add(
      (v)=>{ return typeof(v) === 'number' || typeof(v) === 'object'; },
      (v)=>{ return v.toString(); }
    ).apply(this._value);

    return this;
  };

  /**
   *
   */
  Filters.prototype.trim = function(){
    let f = new Filter();

    if(arguments.length == 1){
      if(typeof arguments[0] === 'string'){
        this._value = f.add(
          (v)=>{ return typeof(v) === 'string'; },
          (v)=>{ return v.replace(new RegExp(`^${arguments[0]}|${arguments[0]}$`, 'gm'), ''); }
        ).apply(this._value);
      }else if(typeof(arguments[0]) === 'object' && (arguments[0] instanceof RegExp)){
        this._value = f.add(
          (v)=>{ return typeof(v) === 'string'; },
          (v)=>{ return v.replace(arguments[0], ''); }
        ).apply(this._value);
      }
    }else if(arguments.length == 2){
      if(typeof(arguments[0]) === 'string' && typeof(arguments[1]) === 'string'){
        this._value = f.add(
          (v)=>{ return typeof(v) === 'string'; },
          (v)=>{ return v.replace(new RegExp(`^${arguments[0]}|${arguments[1]}$`, 'gm'), ''); }
        ).apply(this._value);
      }
    }else{
      this._value = f.add(
        (v)=>{ return typeof(v) === 'string'; },
        (v)=>{ return v.trim(); }
      ).apply(this._value);
    }

    return this;
  };

  /**
   *
   */
  Filters.prototype.number = function(def){
    let f = new Filter();

    this._value = f.add(
      (v)=>{ return v === undefined || v === null; },
      (v)=>{ return typeof(def) === 'number' ? def : 0; }
    ).add(
      (v)=>{ return typeof v === 'boolean'; },
      (v)=>{ return v ? 1 : 0; }
    ).add(
      (v)=>{ return typeof(v) === 'string' && v.trim() == ''; },
      (v)=>{ return typeof(def) === 'number' ? def : 0; }
    ).add(
      (v)=>{ return typeof v === 'string'; },
      (v)=>{ var tmp = new Number(v); return isNaN(tmp) ? (typeof def === 'number' ? def : 0) : tmp.valueOf(); }
    ).add(
      (v)=>{ return typeof(v) === 'object' && v.toString().trim() == '' },
      (v)=>{ return typeof(def) === 'number' ? def : 0; }
    ).add(
      (v)=>{ return typeof v === 'object'; },
      (v)=>{ var tmp = new Number(v.toString()); return isNaN(tmp) ? (typeof def === 'number' ? def : 0) : tmp.valueOf(); }
    ).apply(this._value);

    return this;
  };

  /**
   *
   */
  Filters.prototype.int = function(radix, def){
  
    let f = new Filter();

    this._value = f.add(
      (v)=>{ return v === undefined || v === null; },
      (v)=>{ return typeof(def) === 'number' ? (Number.isInteger(def) ? def : Math.trunc(def)) : 0; }
    ).add(
      (v)=>{ return typeof v === 'boolean'; },
      (v)=>{ return v ? 1 : 0; }
    ).add(
      (v)=>{ return typeof v === 'number'; },
      (v)=>{ return Number.isInteger(v) ? v : Math.trunc(v); }
    ).add(
      (v)=>{ return typeof(v) === 'string' && v.trim() == ''; },
      (v)=>{ return typeof(def) === 'number' ? (Number.isInteger(def) ? def : Math.trunc(def)) : 0; }
    ).add(
      (v)=>{ return typeof v === 'string'; },
      (v)=>{ 
        var tmp = parseInt(v, Number.isInteger(radix) ? radix : 10);
        return isNaN(tmp) ? (typeof def === 'number' ? (Number.isInteger(def) ? def : Math.trunc(def)) : 0) : tmp; }
    ).add(
      (v)=>{ return typeof v === 'object'; },
      (v)=>{ 
        var tmp = parseInt(v.toString(), Number.isInteger(radix) ? radix : 10);
        return isNaN(tmp) ? (typeof def === 'number' ? (Number.isInteger(def) ? def : Math.trunc(def)) : 0) : tmp; }
    ).apply(this._value);

    return this;
  };

  /**
   *
   */
  Filters.prototype.float = function(def){
    let f = new Filter();

    this._value = f.add(
      (v)=>{ return v === undefined || v === null; },
      (v)=>{ return typeof(def) === 'number' ? def : 0; }
    ).add(
      (v)=>{ return typeof v === 'boolean'; },
      (v)=>{ return v ? 1 : 0; }
    ).add(
      (v)=>{ return typeof v === 'string'; },
      (v)=>{
        var tmp = parseFloat(v);
        return isNaN(tmp) ? (typeof def === 'number' ? def : 0) : tmp; }
    ).add(
      (v)=>{ return typeof v === 'object'; },
      (v)=>{
        var tmp = parseFloat(v.toString());
        return isNaN(tmp) ? (typeof def === 'number' ? def : 0) : tmp; }
    ).apply(this._value);

    return this;
  };

  /**
   * Custom filter
   */
  Filters.prototype.custom = function(f, context){
    if(typeof f === 'function') this._value = f.call(context ? context : this, this._value);
    return this;
  };

  /**
   * 
   */
  const retf = function(value){
    return new Filters(value);
  };

  /**
   * Bind
   */
  retf.bind = function(name, f, context){
    Filters.prototype[name] = function(){
      Array.prototype.splice.call(arguments, 0, 0, this._value);
      this._value = f.apply(context ? context : this, arguments);
      return this;
    };
    return retf;
  };

  // Return
  return retf;

})();

