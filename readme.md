# node-value-filter

Providing convenient methods to do fitering to the variables.

## Install
```bash
node install node-value-filter
```

## Usage
```javascript
const filter = require('node-value-filter');

var a = '"123.456"';
var b = filter(a).trim('"').number().int().get();
// b = 123;
```

## Built-in filters
### .string(*default*)
* **undefined** => *default* | ''
* **null**      => *default* | ''
* **true**      => 'true'
* **false**     => 'false'
* ***number***    => ***number***.toString()
* ***object***    => ***object***.toString()
* ***string***    => not changed

##### Parameters:
* **default** string (*optional*)

##### Example:
```javascript
var a = filter(123.456).string().get();
// a = '123.456' (string)
var b  = filter(null).string('-').get();
// b = '-' (string)
```

### .trim()
* ***string*** => ***string***.trim()
* *others* => not changed

##### Example:
```javascript
var a = filter([' abcdef ']).string().trim().get();
// a = 'abcdef' (string)
```

### .trim(*chars*)
* ***string*** => ***string***.replace( /^*chars*|*chars*$/gm, '' )
* *others* => not changed

##### Parameters:
* **chars** string | RegExp

##### Example:
```javascript
var a = filter('"abcdef"').trim('"').get();
// a = abcdef (string)
var b = filter('{abcdef}').trim(/^{|}$/gm).get();
// b = abcdef (string)
```

### .trim(*prefix*, *suffix*)
* ***string*** => ***string***.replace( /^*prefix*|*suffix*$/gm, '' )
* *others* => not changed

##### Parameters:
* **prefix** string
* **suffix** string

##### Example:
```javascript
var a = filter('{abcdef}').trim('{','}').get();
// a = abcdef (string)
```

### .number(*default*)
* **undefined** => *default* | 0
* **null** => *default* | 0
* **true** => 1
* **false** => 0
* ***string*** => new Number( ***string*** ).valueOf() | *default* | 0
* ***object*** => new Number( ***object***.toString() ).valueOf() | *default* | 0
* ***number*** => not changed

##### Parameters:
* **default** number (optional)

##### Example:
```javascript
var a = filter('0xff').number().get();
// a = 255 (number)
var b = filter('abc').number(-1).get();
// b = -1 (number)
```

### .int(*radix*, *default*)
* **undefined** => *default* | 0
* **null** => *default* | 0
* **true** => 1
* **false** => 0
* ***string*** => parseInt( ***string***, *radix* | 10 ) | 0
* ***object*** => parseInt( ***object***.toString(), *radix* | 10 ) | 0
* ***number*** => Math.trunc( ***number*** )

##### Parameters:
* **radix** integer (optional)
* **default** integer (optional)

##### Example:
```javascript
var a = filter([123.456]).string().int().get();
// a = 123 (number)
var b = filter(['ff']).string().int(16).get();
// b = 255 (number)
var c = filter([]).string().int(-1).get();
// c = -1 (number)
```

### .float(*default*)
* **undefined** => *default* | 0
* **null** => *default* | 0
* **true** => 1
* **false** => 0
* ***string*** => parseFloat( ***string*** ) | *default* | 0
* ***object*** => parseFloat( ***object***.toString() ) | *default* | 0
* ***number*** => not changed

##### Parameters:
* **default** number

##### Example:
```javascript
var a = filter(['1e2']).string().float().get();
// a = 100 (number)
var b = filter([]).string().float(0.1).get();
// b = 0.1 (number)
```

## Custom filter

### .custom(*f*, *context*)

##### Parameters:
* **f** function
* **context** object (optional)

##### Example:
```javascript
var a = filter(something).custom((input)=>{
  // Do something with input
  return result;
}).get();
```

### .bind(*name*, *f*, *context*)

##### Paramters:
* **name** string
* **f** function
* **context** object (optional)

##### Exampel:
```javascript
filter
.bind('custom_name', (input, parameters...)=>{
  // Do something with input, parameters...
  return result;
})
.bind('other_name', ...);

var a = filter.custom_name(parameters...).get();
var b = filter.other_name(...).get();
```

## Bugs or feature requests
Please contact to [email](mailto:lchild358@yahoo.com.vn)

(Please add the prefix "[node-value-filter]" into the email title. Thanks!)

## License
[ISC](https://opensource.org/licenses/ISC)
